class CreateInvitados < ActiveRecord::Migration[5.0]
  def change
    create_table :invitados do |t|
      t.string :nombre
      t.text :regalo

      t.timestamps
    end
  end
end
