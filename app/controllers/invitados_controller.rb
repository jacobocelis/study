class InvitadosController < ApplicationController
	def index
    	@invitados = Invitado.all
  	end
	def show
		@invitado = Invitado.find(params[:id])
  	end
	def new
	
	end
	def create
		
		 @invitado = Invitado.new(invitado_params)
		 @invitado.save
		 redirect_to @invitado
	end
	def destroy
		Invitado.destroy(params[:id])
		redirect_to invitados_path
	end
	private
  		def invitado_params
    	params.require(:invitado).permit(:nombre, :regalo)
  		end
end
